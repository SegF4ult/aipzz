//! This module is meant to organize the various sorting algorithms intended to be a part of Aipzz.
mod bubblesort;
pub use crate::sorters::bubblesort::BubbleSort;
mod combsort;
pub use crate::sorters::combsort::CombSort;
mod insertionsort;
pub use crate::sorters::insertionsort::InsertionSort;
