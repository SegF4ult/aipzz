//! The traits module houses traits to indicate for which types sorting can be implemented.

/// The SliceSorter trait is intended to be implemented by a separate struct.
/// For instance, the `aipzz::sorters::BubbleSort` struct.
pub trait SliceSorter {
    /// The sort method takes a mutable reference to a slice of type T and sorts it in-place. Only defined for T: [`Ord`].
    fn sort<T>(&self, slice: &mut [T])
    where
        T: Ord;
}
