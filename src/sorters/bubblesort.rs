use crate::traits::SliceSorter;

/// BubbleSort is a rather inefficient sorting algorithm which walks the slice and swaps elements if needed.
/// [https://en.wikipedia.org/wiki/Bubble_sort](https://en.wikipedia.org/wiki/Bubble_sort)
pub struct BubbleSort;

impl SliceSorter for BubbleSort {
    fn sort<T>(&self, slice: &mut [T])
    where
        T: Ord,
    {
        let mut swapped = true;
        while swapped {
            swapped = false;
            for i in 0..(slice.len() - 1) {
                if slice[i] > slice[i + 1] {
                    slice.swap(i, i + 1);
                    swapped = true;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::BubbleSort;
    use crate::traits::SliceSorter;

    #[test]
    fn test_bubblesort() {
        let mut things = vec![4, 2, 3, 1, 5];
        BubbleSort.sort(&mut things);
        assert_eq!(things, &[1, 2, 3, 4, 5]);
    }
}
