use crate::traits::SliceSorter;

/// CombSort is an optimization of BubbleSort.
/// The idea is to bring smaller elements to the front of the slice quicker, thus avoiding the worst-case scenario for BubbleSort.
/// After some iterations, the behaviour reverts to a basic BubbleSort
/// [https://en.wikipedia.org/wiki/Comb_sort](https://en.wikipedia.org/wiki/Comb_sort)
pub struct CombSort;
const SHRINK: f32 = 1.3;

impl SliceSorter for CombSort {
    fn sort<T>(&self, slice: &mut [T])
    where
        T: Ord,
    {
        let mut sorted = false;
        let mut gap: usize = slice.len();
        let mut i: usize;

        while !sorted {
            gap = (gap as f32 / SHRINK).floor() as usize;
            if gap <= 1 {
                gap = 1;
                sorted = true;
            }

            i = 0;
            while (i + gap) < slice.len() {
                if slice[i] > slice[i + gap] {
                    slice.swap(i, i + gap);
                    sorted = false;
                }
                i += 1;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::CombSort;
    use crate::traits::SliceSorter;

    #[test]
    fn test_combsort() {
        let mut things = vec![4, 2, 3, 1, 5];
        CombSort.sort(&mut things);
        assert_eq!(things, &[1, 2, 3, 4, 5]);
    }
}
