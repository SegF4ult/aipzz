//! This module defines the [`SliceSorter`] implementation for the [`InsertionSort`] struct.
use crate::traits::SliceSorter;

/// This struct is meant to implement [Insertion Sort](https://wikipedia.org/wiki/Insertion_Sort)
pub struct InsertionSort;

impl SliceSorter for InsertionSort {
    fn sort<T>(&self, slice: &mut [T])
    where
        T: Ord,
    {
        // [ sorted | not sorted ]
        for unsorted in 1..slice.len() {
            let mut i = unsorted;
            while i > 0 && slice[i - 1] > slice[i] {
                slice.swap(i - 1, i);
                i -= 1;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::InsertionSort;
    use crate::traits::SliceSorter;

    #[test]
    fn test_insertion_sort() {
        let mut things = vec![4, 2, 3, 1, 5];
        InsertionSort.sort(&mut things);
        assert_eq!(things, &[1, 2, 3, 4, 5]);
    }
}
