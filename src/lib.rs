#![deny(missing_docs)]
//! Time for a slice! Aipzz is a crate of sorters intended to run on slices.
//!
//! This crate provides a SliceSorter trait as well as several implementations of sorting algorithms.
//! Making use of it is as simple as this following example:
//! ```
//! use aipzz::traits::SliceSorter;
//! use aipzz::sorters::BubbleSort;
//! let mut things = vec![5,2,18,9,23,1];
//! BubbleSort.sort(&mut things);
//! assert_eq!(things, &[1, 2, 5, 9, 18, 23]);
//! ```
pub mod traits;
pub mod sorters;

#[cfg(test)]
mod tests {
    use crate::traits::SliceSorter;
    struct StandardSorter;

    impl SliceSorter for StandardSorter {
        fn sort<T>(&self, slice: &mut [T])
        where
            T: Ord,
        {
            slice.sort();
        }
    }

    #[test]
    fn test_standard_sorter() {
        let mut things = vec![4, 2, 3, 1, 5];
        StandardSorter.sort(&mut things);
        assert_eq!(things, &[1, 2, 3, 4, 5]);
    }
}
