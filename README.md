# aipzz

Time for a slice! Aipzz is a crate of sorters intended to run on slices.

This crate provides a SliceSorter trait as well as several implementations of sorting algorithms.
Making use of it is as simple as this following example:
```rust
use aipzz::traits::SliceSorter;
use aipzz::sorters::BubbleSort;
let mut things = vec![5,2,18,9,23,1];
BubbleSort.sort(&mut things);
assert_eq!(things, &[1, 2, 5, 9, 18, 23]);
```
